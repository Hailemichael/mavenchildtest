package integration;

import static org.junit.Assert.assertEquals;

import org.junit.Test;
import org.mule.DefaultMuleMessage;
import org.mule.api.MuleMessage;
import org.mule.api.transport.PropertyScope;
import org.mule.module.client.MuleClient;
import org.mule.tck.junit4.FunctionalTestCase;

public class MavenChildTestITCase extends FunctionalTestCase {

	public static final boolean DEBUG = false;
	
	@Override
	protected String getConfigResources() {
		return "mavenchildtest.xml";
	}
	
	@Test
	public void testIntegrationGetEmployeeById() throws Exception {
        //Set input message payload
        Object payload = null; 
        
        //prepare input message properties and set input message
        final MuleMessage muleMessage = new DefaultMuleMessage(payload, muleContext);
        muleMessage.setProperty("http.method", "GET", PropertyScope.INVOCATION);
                
        //Create Mule Client and send request with input message
        MuleClient muleClient = new MuleClient(muleContext);
	    MuleMessage receivedMessage = muleClient.send("http://localhost:8092/maven?lang=sv", muleMessage);
        if(DEBUG) System.out.println("Recieved Message: " + receivedMessage);
	    
	    
	    assertEquals("Tjena", receivedMessage.getPayloadAsString("ISO-8859-1")); 	    	 
	}
	
	
}
